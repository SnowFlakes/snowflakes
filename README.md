## SnowFlakes

# A powerful collection of 'helpers' to #include in your freebasic projects

# How to use
Ideally clone the repository into your 'FreeBASIC/inc' folder so you can '#include' the desired '.bi' or '.bas' file
easily without taking care about the correct paths, e.g. #include "snowflakes/gfxresize.bas"
The second aspect is that this way you easily can keep the files updated without the need to copy them into every of your projects place again.

# Examples
Every include module comes with an '-example.bas' file that quickly demonstrates what it can do and how it can be used.

## List of include files and their subs/functions

# gfxresize.bas

Note:
 - Currently the subs will only work on Windows Systems (32bit freebasic only)
Subs/Functions:
 - sub gfx.PreResize(iUndo as integer=0)   
 - sub gfx.Resize(iWid as integer=0,iHei as integer=0,iCenter as integer=1,iResizable as integer=0)
 - sub gfx.BorderColor( iR as ulong , iG as ulong , iB as ulong )
