'
' This file is part of the 'snowflakes' collection
' https://codeberg.org/SnowFlakes/snowflakes
'
' Example of Usage
'
#cmdline "-s gui"

#include "snowflakes/gfxresize.bas"

dim as long WantWid=320, WantHei=240 ' <-- the base resolution we want to scale

'works with D2D/DirectX/OpenGL, just not with GDI as there's no hw-scaling available

'N=0 > Default Rendererer (starts windowed,resizable) (D2D?)
'N=1 > OpenGL  Rendererer (starts windowed)
'N=2 > DirectX Rendererer (starts fullscren)

for N as long = 0 to 3
  
  if N=0 then screencontrol(fb.SET_DRIVER_NAME,"GDI")
  if N=1 then screencontrol(fb.SET_DRIVER_NAME,"") 'default?
  if N=2 then screencontrol(fb.SET_GL_2D_MODE,fb.OGL_2D_MANUAL_SYNC)
  if N=3 then screencontrol(fb.SET_GL_2D_MODE,fb.OGL_2D_NONE): screencontrol(fb.SET_DRIVER_NAME,"DirectX")
    
  Gfx.PreResize() ' <-- MUST be called one time before every NEW changes to the window!

  dim as integer iFlags(3) = { fb.gfx_SHAPED_WINDOW , fb.GFX_NO_FRAME , fb.GFX_OPENGL , fb.gfx_fullscreen }
  screenres (WantWid,WantHei,8,,iFlags(N))
  'screenres (WantWid,WantHei,8,,iFlags(N)) or fb.GFX_HIGH_PRIORITY or fb.gfx_no_switch)
  
  if N=0 then
    color 15,0 : cls
    for iY as long = 0 to WantHei-1
      line(0,iY)-(WantWid, iY),3,,iif((iY and 3)<2,&hCCCC,&h3333)      
    next iY      
    circle(WantWid\2,WantHei\2),WantHei\4,0,,,,f
    color 15,17 : gfx.BorderColor(0,0,0) 'it accounts for shaped!!
  elseif N=1 then
    color 0,15 : cls    
    gfx.BorderColor(255,255,255)
  elseif N=3 then
    color 14,1 : cls    
    gfx.BorderColor(0,0,128)
  end if
  
  '-1 makes auto based on the other coordinate
  'so for example: Gfx.Resize(-1,0) would make it will use the full desktop height...
  'with automatic based on informed resoution, (good for windowed)
  Gfx.Resize(-1,WantHei*2) ',,1)
  
  dim as string sDriver
  dim as integer ScrWid,ScrHei
  
  screeninfo ScrWid,ScrHei
  screencontrol(fb.GET_DRIVER_NAME,sDriver)
  locate 2,2 : print sDriver;ScrWid;"x" & ScrHei
  line(0,0)-(ScrWid-1,ScrHei-1),10,b,&hFF00
  line(0,0)-(ScrWid-1,ScrHei-1),6,b,&h00FF
  
  do
    dim as integer MX,MY
    GetMouse MX,MY
    locate 4,2: print MX;",";MY;"    ";
    locate 6,2: print "ALT+ENTER to toggle fullscreen test"
    locate 7,2: print "press SPACE key to continue"
    sleep 1,1 : flip    
  loop until inkey = chr(32)
  
next N

' an example of fading out the border
for N as long = 128 to 0 step -4    
  gfx.BorderColor(0,0,N)  
  screensync  
next N

sleep 500
